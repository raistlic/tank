package org.raistlic.game.tank.exception;

public class ResourceLoadingException extends RuntimeException {

  public ResourceLoadingException(String message) {

    super(message);
  }

  public ResourceLoadingException(String message, Throwable cause) {

    super(message, cause);
  }

  public ResourceLoadingException(Throwable cause) {

    super(cause);
  }
}
