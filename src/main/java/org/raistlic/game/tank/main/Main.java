package org.raistlic.game.tank.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class Main {

  public static void main(String[] args) {

    SwingUtilities.invokeLater(Main::run);
  }

  private static void run() {

    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
    applicationContext.register(MainConfig.class);
    applicationContext.refresh();

    JComponent mainView = applicationContext.getBean(JComponent.class);
    JFrame mainFrame = new JFrame("Tank");
    mainFrame.setContentPane(mainView);
    mainFrame.pack();
    mainFrame.setResizable(false);
    mainFrame.setLocationRelativeTo(null);
    mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    mainFrame.setVisible(true);
  }
}
