package org.raistlic.game.tank.util;

import org.raistlic.game.tank.exception.ResourceLoadingException;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

@Component
public class ResourceLoader {

  public BufferedImage loadClasspathImage(String classPath) {

    try (InputStream in = getClass().getResourceAsStream(classPath)) {
      return ImageIO.read(in);
    }
    catch (IOException ex) {
      throw new ResourceLoadingException(ex);
    }
  }
}
