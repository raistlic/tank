package org.raistlic.game.tank.view;

import java.awt.image.BufferedImage;

public class TankSprite {

  public static final int TANK_SPRITE_COUNT_H = 8;
  public static final int TANK_SPRITE_COUNT_V = 8;

  private BufferedImage image;

  private int width;

  private int height;

  private int cols;

  private int rows;

  public TankSprite(BufferedImage image, int width, int height) {

    assert image != null : "image cannot be null";
    assert width > 0 : "width must be greater than 0";
    assert height > 0 : "height must be greater than 0";

    this.image = image;
    this.width = width;
    this.height = height;
    this.cols = image.getWidth() / width;
    this.rows = image.getHeight() / height;
  }

  public BufferedImage getSprite(int x, int y) {

    assert x >= 0 : "Invalid x: " + x;
    assert y >= 0 : "Invalid x: " + y;

    int dx = x * width;
    int dy = y * height;
    return image.getSubimage(dx, dy, width, height);
  }
}
