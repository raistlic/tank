package org.raistlic.game.tank.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.JComponent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

@Component
public class MainPanel extends JComponent {

  @Autowired
  private SpriteManager spriteManager;

  public MainPanel() {
    super.setPreferredSize(new Dimension(512, 512));
  }

  @Override
  protected void paintComponent(Graphics g) {

    g.setColor(Color.BLACK);
    g.fillRect(0, 0, getWidth(), getHeight());

    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        g.drawImage(spriteManager.getPlayerOneSprite().getSprite(j, i), 32 * j + 50, 32 * i + 50, null);

      }
    }
  }
}
