package org.raistlic.game.tank.view;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.raistlic.game.tank.util.ResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

@Component
public class SpriteManager {

  @Value("${view.sprite.path}")
  private String spritePath;

  @Value("${view.sprite.scale}")
  private float spriteScale;

  @Value("${view.sprite.unit.width}")
  private int spriteUnitWidth;

  @Value("${view.sprite.unit.height}")
  private int spriteUnitHeight;

  @Autowired
  private ResourceLoader resourceLoader;

  @Getter
  private TankSprite playerOneSprite;

  @Getter
  private TankSprite playerTwoSprite;

  @Getter
  private TankSprite enemyOneSprite;

  @Getter
  private TankSprite enemyTwoSprite;

  @EventListener(ContextRefreshedEvent.class)
  public void loadSprites(ContextRefreshedEvent contextRefreshedEvent) {

    System.out.println("spriteScale: " + spriteScale);
    System.out.println("spriteUnitWidth: " + spriteUnitWidth);
    System.out.println("spriteUnitHeight: " + spriteUnitHeight);

    BufferedImage spriteImage = resourceLoader.loadClasspathImage(spritePath);

    playerOneSprite = createTankSprite(0, 0, spriteImage);
    playerTwoSprite = createTankSprite(0, TankSprite.TANK_SPRITE_COUNT_V, spriteImage);
    enemyOneSprite = createTankSprite(TankSprite.TANK_SPRITE_COUNT_H, 0, spriteImage);
    enemyTwoSprite = createTankSprite(TankSprite.TANK_SPRITE_COUNT_H, TankSprite.TANK_SPRITE_COUNT_V, spriteImage);
  }

  private TankSprite createTankSprite(int x, int y, BufferedImage spriteImage) {

    int dx = spriteUnitWidth * x;
    int dy = spriteUnitHeight * y;
    int width = spriteUnitWidth * TankSprite.TANK_SPRITE_COUNT_H;
    int height = spriteUnitHeight * TankSprite.TANK_SPRITE_COUNT_V;

    BufferedImage originalImage = spriteImage.getSubimage(dx, dy, width, height);
    BufferedImage image = new BufferedImage(
        (int) (width * spriteScale),
        (int) (height * spriteScale),
        BufferedImage.TYPE_INT_RGB);

    AffineTransform at = new AffineTransform();
    at.scale(2.0, 2.0);

//    Graphics2D g = image.createGraphics();
//    g.setTransform(at);
//    g.drawImage(originalImage, 0, 0, null);
//    g.dispose();


    AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
    image = scaleOp.filter(originalImage, image);
    return new TankSprite(image, (int) (spriteScale * spriteUnitWidth), (int) (spriteScale * spriteUnitHeight));
//    return new TankSprite(originalImage, spriteUnitWidth, spriteUnitHeight);
  }
}
